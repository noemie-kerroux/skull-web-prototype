# Skull Web Prototype

Skull Web Prototype is a responsive integrative starter theme.

## Demo

You can see the demo of this prototype by clicking here :
[http://demos.potatolab.fr/skull-web-prototype/](http://demos.potatolab.fr/skull-web-prototype/)

## Authors

* **Noémie Kerroux** - *Initial work* - [noemiekerroux.fr](http://noemiekerroux.fr)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

